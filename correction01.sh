#!/bin/bash

# Colors
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Functions to display success or failure
function success {
    echo -e "${GREEN}$1${NC}"
}

function failure {
    echo -e "${RED}$1${NC}"
}

# Check if Apache is installed
if apache2 -v > /dev/null 2>&1; then
    success "Apache2 is installed"
else
    failure "Apache2 is not installed"
fi

# Check if Apache is listening on port 8080 and serving the correct content
if curl -s http://localhost:8080 | grep -iq "Hello 8080"; then
    success "Apache2 is serving 'Hello 8080' on port 8080"
else
    failure "Apache2 is not serving 'Hello 8080' on port 8080"
fi

# Check if Apache is listening on port 8081 and serving the correct content
if curl -s http://localhost:8081 | grep -iq "Hello 8081"; then
    success "Apache2 is serving 'Hello 8081' on port 8081"
else
    failure "Apache2 is not serving 'Hello 8081' on port 8081"
fi

# Check if Docker image exists
if docker images | grep -iq "apache-ifapme"; then
    success "Docker image 'apache-ifapme' exists"
else
    failure "Docker image 'apache-ifapme' does not exist"
fi

# Check if Docker container is running and serving the correct content
if curl -s http://localhost | grep -iq "Hello Docker"; then
    success "Docker container is serving 'Hello Docker'"
else
    failure "Docker container is not serving 'Hello Docker'"
fi

# Check UFW rules
SSH_RULE=$(sudo ufw status | grep -iq "OpenSSH" || sudo ufw status | grep -iq "22" && echo "exists" || echo "missing")
NFS_RULE=$(sudo ufw status | grep -iq "2049/tcp" || sudo ufw status | grep -iq "2049" && echo "exists" || echo "missing")
HTTP_RULE=$(sudo ufw status | grep -iq "80/tcp" || sudo ufw status | grep -iq "80" && echo "exists" || echo "missing")
PORT_8080_RULE=$(sudo ufw status | grep -iq "8080/tcp" || sudo ufw status | grep -iq "8080" && echo "exists" || echo "missing")
PORT_8081_RULE=$(sudo ufw status | grep -iq "8081/tcp" || sudo ufw status | grep -iq "8081" && echo "exists" || echo "missing")

if [ "$SSH_RULE" == "exists" ]; then
    success "UFW allows SSH"
else
    failure "UFW does not allow SSH"
fi

if [ "$NFS_RULE" == "exists" ]; then
    success "UFW allows NFS"
else
    failure "UFW does not allow NFS"
fi

if [ "$HTTP_RULE" == "exists" ]; then
    success "UFW allows HTTP (port 80)"
else
    failure "UFW does not allow HTTP (port 80)"
fi

if [ "$PORT_8080_RULE" == "exists" ]; then
    success "UFW allows port 8080"
else
    failure "UFW does not allow port 8080"
fi

if [ "$PORT_8081_RULE" == "exists" ]; then
    success "UFW allows port 8081"
else
    failure "UFW does not allow port 8081"
fi

# Check if ramdisk is mounted
if mount | grep -iq "/srv/ram_disk"; then
    success "Ramdisk is mounted at /srv/ram_disk"
else
    failure "Ramdisk is not mounted at /srv/ram_disk"
fi

# Check if NFS server is serving the correct directory
if sudo exportfs -v | grep -iq "/srv/nfs_ifapme"; then
    success "NFS is serving /srv/nfs_ifapme"
else
    failure "NFS is not serving /srv/nfs_ifapme"
fi

# Check if users exist and have the correct settings
if id "ifapme-test" &>/dev/null; then
    if [[ $(getent passwd ifapme-test | cut -d: -f7) == "/bin/bash" ]]; then
        success "User ifapme-test exists with /bin/bash shell"
    else
        failure "User ifapme-test exists but does not have /bin/bash shell"
    fi
else
    failure "User ifapme-test does not exist"
fi

if id "ifapme-locked" &>/dev/null; then
    if [[ $(getent passwd ifapme-locked | cut -d: -f7) == "/usr/sbin/nologin" ]]; then
        success "User ifapme-locked exists with /usr/sbin/nologin shell"
    else
        failure "User ifapme-locked exists but does not have /usr/sbin/nologin shell"
    fi
else
    failure "User ifapme-locked does not exist"
fi

cat /etc/machine-id
